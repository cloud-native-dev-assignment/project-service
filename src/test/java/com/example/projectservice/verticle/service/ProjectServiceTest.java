package com.example.projectservice.verticle.service;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Set;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.example.projectservice.model.Project;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class ProjectServiceTest extends MongoTestBase {

    private Vertx vertx;

    @Before
    public void setup(TestContext context) throws Exception {
        vertx = Vertx.vertx();
        vertx.exceptionHandler(context.exceptionHandler());
        JsonObject config = getConfig();
        mongoClient = MongoClient.createNonShared(vertx, config);
        Async async = context.async();
        dropCollection(mongoClient, "projects", async, context);
        async.await(10000);
    }

    @After
    public void tearDown() throws Exception {
        mongoClient.close();
        vertx.close();
    }

    @Test
    public void testAddProject(TestContext context) throws Exception {
        String projectId = "999999";
        String name = "project999999";
        Project project = new Project();
        project.setProjectId(projectId);
        project.setProjectName("project999999");

        ProjectService service = new ProjectServiceImpl(vertx, getConfig(), mongoClient);

        Async async = context.async();

        service.addProject(project, ar -> {
            if (ar.failed()) {
                context.fail(ar.cause().getMessage());
            } else {
                JsonObject query = new JsonObject().put("_id", projectId);
                mongoClient.findOne("projects", query, null, ar1 -> {
                    if (ar1.failed()) {
                        context.fail(ar1.cause().getMessage());
                    } else {
                        assertThat(ar1.result().getString("projectName"), equalTo(name));
                        async.complete();
                    }
                });
            }
        });
    }

    @Test
    public void testGetProjects(TestContext context) throws Exception {
        Async saveAsync = context.async(2);
        String id1 = "111111";
        JsonObject json1 = new JsonObject()
                .put("projectId", id1)
                .put("projectName", "project111111");
                

        mongoClient.save("projects", json1, ar -> {
            if (ar.failed()) {
                context.fail();
            }
            saveAsync.countDown();
        });

        String id2 = "222222";
        JsonObject json2 = new JsonObject()
                .put("projectId", id2)
                .put("projectName", "project222222");
                

        mongoClient.save("projects", json2, ar -> {
            if (ar.failed()) {
                context.fail();
            }
            saveAsync.countDown();
        });

        saveAsync.await();

        ProjectService service = new ProjectServiceImpl(vertx, getConfig(), mongoClient);

        Async async = context.async();

        service.getProjects(ar -> {
            if (ar.failed()) {
                context.fail(ar.cause().getMessage());
            } else {
                assertThat(ar.result(), notNullValue());
                assertThat(ar.result().size(), equalTo(2));
                Set<String> ids = ar.result().stream().map(p -> p.getProjectId()).collect(Collectors.toSet());
                assertThat(ids.size(), equalTo(2));
                assertThat(ids, allOf(hasItem(id1),hasItem(id2)));
                async.complete();
            }
        });
    }

    // @Test
    // public void testGetProduct(TestContext context) throws Exception {
    //     Async saveAsync = context.async(2);
    //     String itemId1 = "111111";
    //     JsonObject json1 = new JsonObject()
    //             .put("itemId", itemId1)
    //             .put("name", "productName1")
    //             .put("desc", "productDescription1")
    //             .put("price", new Double(100.0));

    //     mongoClient.save("products", json1, ar -> {
    //         if (ar.failed()) {
    //             context.fail();
    //         }
    //         saveAsync.countDown();
    //     });

    //     String itemId2 = "222222";
    //     JsonObject json2 = new JsonObject()
    //             .put("itemId", itemId2)
    //             .put("name", "productName2")
    //             .put("desc", "productDescription2")
    //             .put("price", new Double(100.0));

    //     mongoClient.save("products", json2, ar -> {
    //         if (ar.failed()) {
    //             context.fail();
    //         }
    //         saveAsync.countDown();
    //     });

    //     saveAsync.await();

    //     CatalogService service = new CatalogServiceImpl(vertx, getConfig(), mongoClient);

    //     Async async = context.async();

    //     service.getProduct("111111", ar -> {
    //         if (ar.failed()) {
    //             context.fail(ar.cause().getMessage());
    //         } else {
    //             assertThat(ar.result(), notNullValue());
    //             assertThat(ar.result().getItemId(), equalTo("111111"));
    //             assertThat(ar.result().getName(), equalTo("productName1"));
    //             async.complete();
    //         }
    //     });
    // }

    // @Test
    // public void testGetNonExistingProduct(TestContext context) throws Exception {
    //     Async saveAsync = context.async(1);
    //     String itemId1 = "111111";
    //     JsonObject json1 = new JsonObject()
    //             .put("itemId", itemId1)
    //             .put("name", "productName1")
    //             .put("desc", "productDescription1")
    //             .put("price", new Double(100.0));

    //     mongoClient.save("products", json1, ar -> {
    //         if (ar.failed()) {
    //             context.fail();
    //         }
    //         saveAsync.countDown();
    //     });

    //     saveAsync.await();

    //     CatalogService service = new CatalogServiceImpl(vertx, getConfig(), mongoClient);

    //     Async async = context.async();

    //     service.getProduct("222222", ar -> {
    //         if (ar.failed()) {
    //             context.fail(ar.cause().getMessage());
    //         } else {
    //             assertThat(ar.result(), nullValue());
    //             async.complete();
    //         }
    //     });
    // }

    @Test
    public void testPing(TestContext context) throws Exception {
        ProjectService service = new ProjectServiceImpl(vertx, getConfig(), mongoClient);

        Async async = context.async();
        service.ping(ar -> {
            assertThat(ar.succeeded(), equalTo(true));
            async.complete();
        });
    }

}
