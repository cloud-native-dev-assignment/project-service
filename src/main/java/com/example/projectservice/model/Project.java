package com.example.projectservice.model;

import java.io.Serializable;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject
public class Project implements Serializable {

    private static final long serialVersionUID = -4363528255355077649L;
    private String projectId;
    private String ownerFirstName;
    private String ownerLastName;
    private String ownerEmail;
    private String projectName;
    private String projectDesc;
    private String projectStatus;

    public Project() {

    }
    public Project(JsonObject json) {
        this.projectId = json.getString("projectId");
        this.ownerFirstName = json.getString("ownerFirstName");
        this.ownerLastName = json.getString("ownerLastName");
        this.ownerEmail = json.getString("ownerEmail");
        this.projectName = json.getString("projectName");
        this.projectDesc =  json.getString("projectDesc");
        this.projectStatus =  json.getString("projectStatus");
    }

    

    public JsonObject toJson() {

        final JsonObject json = new JsonObject();
        json.put("projectId", this.projectId);
        json.put("ownerFirstName", this.ownerFirstName);
        json.put("ownerLastName", this.ownerLastName);
        json.put("ownerEmail", this.ownerEmail);
        json.put("projectName",this.projectName);
        json.put("projectDesc",this.projectDesc);
        json.put("projectStatus",this.projectStatus);
        return json;
    }

    /**
     * @return the projectStatus
     */
    public String getProjectStatus() {
        return projectStatus;
    }

    /**
     * @param projectStatus the projectStatus to set
     */
    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    /**
     * @return the projectDesc
     */
    public String getProjectDesc() {
        return projectDesc;
    }

    /**
     * @param projectDesc the projectDesc to set
     */
    public void setProjectDesc(String projectDesc) {
        this.projectDesc = projectDesc;
    }

    /**
     * @return the projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * @return the ownerEmail
     */
    public String getOwnerEmail() {
        return ownerEmail;
    }

    /**
     * @param ownerEmail the ownerEmail to set
     */
    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    /**
     * @return the ownerLastName
     */
    public String getOwnerLastName() {
        return ownerLastName;
    }

    /**
     * @param ownerLastName the ownerLastName to set
     */
    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    /**
     * @return the ownerFirstName
     */
    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    /**
     * @param ownerFirstName the ownerFirstName to set
     */
    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    

}
