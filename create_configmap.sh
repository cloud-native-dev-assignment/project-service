#!/bin/sh
oc create configmap project-service --from-file=etc/app-config.yml
oc policy add-role-to-user view -z default
