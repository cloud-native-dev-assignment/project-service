# Project Service

Project Service is is RESTful API application for query freelancers information from NoSQL database (MongoDB)

Project Service use following main technology
- Vert.X
- MongoDB
- JUnit
- Vert.X Embedded MongoDB
- Fabric8
- ConfigMap

## Test with JUnit

This service used JUnit and Vert.X Embedded MongoDB for testing

```
mvn test
```

## Deploy to OpenShift

Fabric8 plugin can be used to deploy to OpenShfit with S2I by
```
mvn fabric8:deploy -Popenshift
```

### API document

Following URI are supported

| Method | URI                                   |  Description  |             
|--------|---------------------------------------|-------------------| 
| GET    | /projects                     |  get all projects           |
| POST   | /projects                     |  create project from JSON   |
| GET    | /projects/{projectId}         |  get project by ID          |
| GET    | /projects/status/{status}     |  get project by status      |




## Test on OpenShift

Following URLs are available for test API Gateway Service on OpenShift

```
curl http://project-service-homework.apps.rhpds311.openshift.opentlc.com/projects
curl  http://project-service-homework.apps.rhpds311.openshift.opentlc.com/projects/{projectId}
curl -X POST http://project-service-homework.apps.rhpds311.openshift.opentlc.com/projects @<JSON file>
```
Following data can be used for test
- Projecgt ID: 1,2,3,4,5
- Project status: open,completed,cancelled

## Authors

* **Voravit** 
